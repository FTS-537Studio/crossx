# 关于CrossX

## 简介
**CrossX** 是一个提供跨语言通信的库，
目前支持的编程语言有：

1. **C++**
2. **C**
3. **Golang** *（通过Cgo调用C/C++接口）*
4. **Python** *（通过Ctypes库调用DLL文件）*

---

## 如何编译

使用以下命令将源码编译为动态链接库（Dynamic-Link Library）：

**Windows**

`g++ crossx.cpp -fPIC -shared -std=c++17 -o crossx.dll`

**Linux**

`g++ crossx.cpp -fPIC -shared -std=c++17 -o crossx.so`


---

## 使用示例

1. **C++**

```cpp
//test.cpp
#include <iostream>
#include <windows.h>
#include "crossx.hpp"
using namespace std;

int main(){
    add_fork_("cpp/main");//注册id
    send_msg_("hello, py!", "py/main")//发送信息至py/main
    Sleep(200);//等待回应
    msg_ msg = read_();//读取信息
    cout << msg[0].first << endl//输出第一条消息的发送者
         << msg[0].second;      //输出第一条消息的内容
    //msg_类型实质是vector<pair<string, string>>，即消息列表
    //msg[i]指的是第i个消息 .first为发送者 .second为具体内容
    return 0;
}
```

编译命令：
`g++ test.cpp -L./ lcrossx -std=c++17 -o test.exe`
<un>
<un>
<un>

2. **C**

```c
//test.c
#include <stdio.h>
#include <windows.h>
#include "crossx.h"

int main(){
    AddFork("c/main");//注册id
    Msg msg;
    msg.msg = "hello go!";//设置信息
    msg.to = "golang/main";//设置接收者
    SendMsg(msg);//发送
    Sleep(200);//等待回复
    Msg mlist[10];//新建一个数组用于接收消息
    Read(mlist, -1);//读入消息
    //Read(Msg* msg, int max_num)
    /*
    Read函数接收两个参数，一个是消息数组，一个是读取的消息数，
    当设置读取消息数为-1时，则代表全部读取
    */
    printf("%s %s", mlist[0].sender, mlist[0].msg);
    //输出第一条消息的 发送者和消息内容
}
```

编译命令：
`gcc test.c -L./ lcrossx -o test.exe`
<un>
<un>
<un>

3. **Golang**

```go
//test.go
package main

/*
	#cgo CFLAGS: -I ./
	#cgo LDFLAGS: ./crossx.dll
	#include "./crossx.h"
*/
import "C"
import "fmt"

func main() {
	C.AddFork(C.CString("golang/main"))//注册id
	var msg C.Msg
	msg.msg = C.CString("hello c!")//设置信息
	msg.to = C.CString("c/main")//设置接收者
	C.SendMsg(msg)//发送
}
```

编译命令：
`go build test.go`
<un>
<un>
<un>

4. **Python**

```python
#test.py
from ctypes import *
import time

dll = CDLL('./crossx.dll', winmode=0)

dll.AddFork(b'py/main')#注册id

class Msg(Structure):#python映射Msg结构体
    _fields_ = [
        ("sender", c_char_p),
        ("msg", c_char_p),
        ("to", c_char_p)
    ]

msg = Msg(msg = b'hello go!', to = b'golang/main')#设置信息

dll.SendMsg(msg)#发送

time.sleep(1)#等待

msg = Msg()
dll.Read(msg, -1)#读取信息

print(str(msg.msg))
```

运行命令：
`python test.py`

*（其他具体用法见文档）*

---

## 文档

相关文档正在编写中...