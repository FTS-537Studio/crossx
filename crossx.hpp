#ifndef CROSS_X_
#define CROSS_X_

#include <iostream>
#include <fstream>
#if __cplusplus < 201703L
	#error "Compilation in languages below C++17 is not supported"
#endif	
#include <filesystem>
#include <vector>

using namespace std;
using namespace std::filesystem;
  
using msg_ = vector<pair<string, string>>;

string root = "./root";
string fork = "";

void init_();
void set_root_(string root_);
void add_fork_(string fork_);
void send_msg_(string msg, string to);
msg_ read_();

#endif
