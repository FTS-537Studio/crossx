//CPP接口

#include "./crossx.hpp"

void init_(){
	path p(root);//删除root目录及其子目录
	remove_all(p);
	
	create_directory(p);//创建一个新的root空目录
}

void set_root_(string root_){
	root = root_;
}

void add_fork_(string fork_){
	path p(root);//新建目录
	p = p / fork_;
	create_directories(p);
	
	ofstream f(p / "self");//添加self文件
	f.close();
	
	fork = fork_;
}

void send_msg_(string msg, string to){
	path p(root);
	p = p / to / "self";
	
	ofstream f(p, ios::app);
	f << fork << endl << msg << endl;
	f.close();
}

msg_ read_(){
	path p(root);
	p = p / fork / "self";
	
	ifstream f(p);
	
	msg_ tmp;
	
	string sender, msg;
	while(getline(f, sender)){
		getline(f, msg);
		
		tmp.push_back(make_pair(sender, msg));
	}
	
	f.close();
	
	ofstream clear(p); 
	clear.close();
	
	return tmp;
}

//定义C接口

extern "C"{
	
	#include "./crossx.h"
	
	void Init(){ init_(); }
	
	void SetRoot(char* root_){ set_root_(root_); }
	char* GetRoot() { return (char*)root.c_str(); }
	
	void AddFork(char* fork_){ add_fork_(fork_); }
	char* GetFork() { return (char*)fork.c_str(); }
	
	void SendMsg(Msg &msg){
		msg.sender = (char*)fork.c_str();
		send_msg_(msg.msg, msg.to);
	}
	int Read(Msg* msg, int max_num){
		msg_ tmp = read_();
		
		int bufsize_ = ((max_num < 0) || (max_num > tmp.size()))
						? tmp.size() : max_num;
		
		for(int i = 0; i<bufsize_; i++){
			msg[i].sender = (char*)tmp[i].first.c_str();
			msg[i].msg = (char*)tmp[i].second.c_str();
			msg[i].to = (char*)fork.c_str();
		}
		
		return bufsize_;
	}
	
}
