#ifndef CROSSX_
#define CROSSX_

typedef struct{
	char *sender;
	char *msg;
	char *to;
} Msg;

void Init();

void SetRoot(char* root_);
char* GetRoot();

void AddFork(char* fork_);
char* GetFork();

void SendMsg(Msg &msg);
int Read(Msg* msg, int max_num);

#endif
